# overlord-boilerplate

This is a simple gulp boilerplate that gives you the following:

* express server w/ livereload browser refresh on change
* sass compilation
* js compilation
* image minifying
* static file watch

It's aim is purely to act as a simple starting point for web projects.

* * *

## Usage

As a prerequisite it's assumed you have installed [node.js](http://nodejs.org). To start using the boilerplate you need to run the following terminal commands.
  
1. clone the repo `git clone https://`your-username`@bitbucket.org/ejcorcoran/overlord-boilerplate.git`
  
2. cd to the boilerplate directory and install dependencies `npm install`
  
3. start gulp with `gulp`
  
4. start hacking away on the contents of `src/` with super fast livereload goodness.
  
* * *